package com.example.transactionaltask;

import com.example.transactionaltask.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class TransactionalTaskApplication implements CommandLineRunner {
    private final StudentService studentService;

    public static void main(String[] args) {
        SpringApplication.run(TransactionalTaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        studentService.saveStudent();
    }
}
