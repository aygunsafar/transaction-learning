package com.example.transactionaltask.service;

import com.example.transactionaltask.models.Student;

public interface StudentService {
    Student createStudent(Student student);
    void saveStudent() throws Exception;
}
