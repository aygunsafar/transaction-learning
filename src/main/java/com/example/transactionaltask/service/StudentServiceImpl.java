package com.example.transactionaltask.service;

import com.example.transactionaltask.annotation.Transactional1;
import com.example.transactionaltask.models.Student;
import com.example.transactionaltask.repo.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Student createStudent(Student student) {
      return studentRepository.save(student);
    }


    @Override
    @Transactional1
    public void saveStudent() throws Exception{

        Student student =Student.builder()
                .age(24)
                .name("Sema")
                .surname("Saidova")
                .build();

        studentRepository.save(student);

        if(true)
            throw new Exception("Something happened");

        Student student2 =Student.builder()
                .age(26)
                .name("Ahmed")
                .surname("Ahmedov")
                .build();

        studentRepository.save(student2);
    }
}
