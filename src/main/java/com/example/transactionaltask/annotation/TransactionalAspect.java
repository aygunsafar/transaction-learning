package com.example.transactionaltask.annotation;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.transaction.Transaction;
import jakarta.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.internal.TransactionManagement;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.lang.reflect.Method;

@Aspect
@Component
@RequiredArgsConstructor
public class TransactionalAspect {


    private final PlatformTransactionManager transactionManager;

    private final EntityManagerFactory entityManagerFactory;


    @Around("@annotation(com.example.transactionaltask.annotation.Transactional1)")
    public Object transactionalAroundAdvise(ProceedingJoinPoint joinPoint) throws Throwable {

        TransactionStatus transactionStatus = null;

        try {

            transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

            Object result = joinPoint.proceed();

            transactionManager.commit(transactionStatus);
            System.out.println("transaction committed");

            return result;

        } catch (Exception ex) {
            if (ex instanceof RuntimeException) {
                transactionManager.rollback(transactionStatus);
                System.out.println("transaction rollback");
            } else {
                transactionManager.commit(transactionStatus);
            }
            throw ex;
        }
    }

    //@TODO -->  code is not working properly, correct it !

 //   @Around("@annotation(com.example.transactionaltask.annotation.Transactional1)")
    public Object transactionalAroundAdvise2(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            System.out.println("Before method invoction");
            Object result = proceedingJoinPoint.proceed();
            System.out.println("After method invocation");
            transaction.commit();
            System.out.println("Transaction committed");
            return result;
        } catch (RuntimeException e) {
            if (transaction.isActive()) {
                transaction.rollback();
                System.out.println("Transaction rollback");
            }
            throw e;
        } finally {
            entityManager.close();
        }


    }
}

