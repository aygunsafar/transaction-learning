package com.example.transactionaltask.repo;

import com.example.transactionaltask.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository  extends JpaRepository<Student,Long> {
}
